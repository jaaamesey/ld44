{
 "music_file_names": [
  "dummy.ogg"
 ],
 "bg_file_names": [
  "black.jpg",
  "black.jpg.import",
  "black.png",
  "black.png.import",
  "white.png",
  "white.png.import"
 ],
 "char_file_names": [
  "john.png",
  "john.png.import",
  "jonathan.png",
  "jonathan.png.import",
  "maddox.png",
  "maddox.png.import",
  "shepard.png",
  "shepard.png.import",
  "woolf.png",
  "woolf.png.import"
 ]
}
