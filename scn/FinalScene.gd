extends Node2D

export var dialogue_to_show := "6"
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	SpecialSong.stop()
	DialogueUI.show_dialogue(dialogue_to_show)
	
