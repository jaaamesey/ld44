extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var scene_path := ""
var swapping_scene := false
func _process(delta):
	if swapping_scene:
		$ColorRect.color.a += delta
		if $ColorRect.color.a >= 1:
			if GameManager.player != null and is_instance_valid(GameManager.player):
				GameManager.total_money += GameManager.player.money
			get_tree().change_scene(scene_path)
			swapping_scene = false
			
	else:
		$ColorRect.color.a -= 2 * delta
	$ColorRect.color.a = clamp($ColorRect.color.a, 0, 1)
	
func swap_scene(path : String, set_checkpoint := true):
	scene_path = path
	swapping_scene = true
	GameManager.current_level_path = path
	
	