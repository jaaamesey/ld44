class_name DialogueBox
extends Control

onready var config_control = get_parent().owner.get_node("MenuLayer/Config")
onready var log_control = get_parent().owner.get_node("MenuLayer/Log")

const DEFAULT_DIALOGUE_SPD : float = 45.0

# Timers (all timers are measured in seconds)
const SKIP_DIALOGUE_TIMER_TIME : float = 1.0/30.0

var pause_timer : float = 0

var is_externally_paused : bool = false

var block_log_on_debug : bool = true
var fast_forward_on_skip : bool = true
var skip_fast_forward_spd : float = 5.0

var auto_go_to_next : bool = false
var append_current_block : bool = false
var append_next_block : bool = false

var visible_characters : float = 0.0 # Float to allow for slower speeds

# Timers
var skip_dialogue_timer : float = 0.0

# Note: bbcode_text should only be used when getting text from the dialogue manager itself

func _ready() -> void:
	visible = false
	DialogueManager.dialogue_box = self

	# Force visibility refresh to prevent size-related bugs
	$DialogueRichTextLabel.visible = false
	$DialogueRichTextLabel.visible = true

	$DialogueRichTextLabel.bbcode_text = get_current_bbcode_text()
	$CharacterLabel.text = ""
	$DoneIndicator.visible = false
	$SkipIndicator.visible = false

	$NextButton.connect("button_up", self, "on_next_pressed")
	DialogueManager.connect("dialogue_change", self, "handle_new_dialogue")

	set_process(false)
	visible = true
	yield(get_tree().create_timer(0.75), "timeout")
	set_process(true)

func _process(delta: float) -> void:
	# Clamp delta to prevent skipping punctuation slowdown
	delta = clamp(delta, 0, Engine.time_scale * 1.0/60.0)
	
	var is_keyboard_skip_held := Input.is_key_pressed(KEY_CONTROL) and Input.is_mouse_button_pressed(BUTTON_LEFT)
	var is_gamepad_skip_held := Input.is_action_pressed("skip") and Input.is_action_pressed("next_dialogue")
	var is_skipping : bool = visible and (is_keyboard_skip_held or is_gamepad_skip_held)

	if is_blocked():
		is_skipping = false

	# Quick hack for making time go faster when skipping dialogue
	$SkipIndicator.visible = false
	#Engine.set_time_scale(1)

	if is_externally_paused:
		return

	if is_skipping:
		$SkipIndicator.visible = true
		#if fast_forward_on_skip:
			#Engine.set_time_scale(skip_fast_forward_spd)

	if pause_timer > 0:
		$DoneIndicator.visible = false
		pause_timer -= delta
		return
		
	if $DialogueRichTextLabel.visible_characters < $DialogueRichTextLabel.text.length():
		$DoneIndicator.visible = false
		if visible:
			tick_dialogue(delta)
	else:
		if auto_go_to_next or (Input.is_action_just_released("next_dialogue") and !is_blocked()):
			auto_go_to_next = false
			on_next_pressed()
		if !auto_go_to_next:
			$DoneIndicator.visible = true

	if is_skipping:
		if skip_dialogue_timer <= 0:
			on_next_pressed(false)
			$DialogueRichTextLabel.visible_characters = $DialogueRichTextLabel.bbcode_text.length()
			skip_dialogue_timer = SKIP_DIALOGUE_TIMER_TIME * skip_fast_forward_spd

	if skip_dialogue_timer > -1:
		skip_dialogue_timer -= delta

func tick_dialogue(delta : float) -> void:
	if DialogueManager.get_current_text().strip_edges() == "" or DialogueManager.get_current_text().empty():
		on_next_pressed(false, true)
		return

	var tick_amount : float = 1

	# Handle slowing down at punctuation
	var prev_char_index : int = $DialogueRichTextLabel.visible_characters
	var prev_char : String = $DialogueRichTextLabel.text[prev_char_index - 1]

	match prev_char:
		',':
			tick_amount = 0.1
		';':
			tick_amount = 0.1
		':':
			tick_amount = 0.1
		'.':
			if prev_char_index < $DialogueRichTextLabel.text.length() and $DialogueRichTextLabel.text[prev_char_index] == ' ':
				tick_amount = 0.04
		'-':
			if prev_char_index < $DialogueRichTextLabel.text.length() and $DialogueRichTextLabel.text[prev_char_index] == ' ':
				tick_amount = 0.04
		'!':
			tick_amount = 0.04
		'?':
			tick_amount = 0.04

	# Special case for not pausing at Mr. and Mrs. and stuff
	# 2 substrings have to be used because for some reason the bottom one doesn't pick up on
	# two letter strings. Yes, this is a hack.
	# TODO: Make this less hacky.
	var prefix_substr3 : String = $DialogueRichTextLabel.text.substr(prev_char_index - 3, 3)
	var prefix_substr4 : String = $DialogueRichTextLabel.text.substr(prev_char_index - 4, 3)

	# Search for nearby prefixes
	var prefix_found : bool = false
	for prefix in ["Mr", "Mrs", "Dr", "Ms", "Sgt", "Lt"]:
		if prefix in prefix_substr3 or prefix in prefix_substr4:
			prefix_found = true

	# Go at normal speed if a prefix is found
	if prefix_found:
		tick_amount = 1

	# Go at normal speed if just starting dialogue
	if visible_characters <= 1:
		tick_amount = 1

	visible_characters += tick_amount * DEFAULT_DIALOGUE_SPD * delta

	var current_char : String = $DialogueRichTextLabel.text[int($DialogueRichTextLabel.visible_characters) - 1]
	var next_char : String = $DialogueRichTextLabel.text[int($DialogueRichTextLabel.visible_characters)]
	# Play blip
	if $DialogueRichTextLabel.visible_characters != int(visible_characters):
		if current_char != ' ':
			$Sounds/blip.play()
	
	var chars_until_done : int = len($DialogueRichTextLabel.text) - $DialogueRichTextLabel.visible_characters
	if next_char == " " and chars_until_done <= 1:
		visible_characters = len($DialogueRichTextLabel.text) + 1
		
	
	$DialogueRichTextLabel.visible_characters = visible_characters

	# Skip next dialogue if empty
	if DialogueManager.has_valid_tail() and (DialogueManager.get_current_text().strip_edges() == "" or DialogueManager.get_current_text().empty()):
		on_next_pressed()
		return


func on_next_pressed(play_sound := true, force := false) -> void:
	if !force and $DialogueRichTextLabel.visible_characters < $DialogueRichTextLabel.text.length():
		return

	if DialogueManager.is_branching:
		return

	var is_previous_dialogue_blank : bool = DialogueManager.get_current_text() == ""
	var is_previous_appended_to : bool = append_next_block
	var is_previous_auto : bool = auto_go_to_next

	DialogueManager.go_to_next_dialogue()
	handle_new_dialogue()
	if play_sound and (DialogueManager.is_branching or !is_previous_dialogue_blank):
		if !is_previous_auto and !is_previous_appended_to:
			$Sounds/click.play(0.01)

func handle_new_dialogue() -> void:
	if DialogueManager.is_branching: # Keep existing text if branching
		return

	# Handle individual dialogue logic stuff here
	var extra_data : Dictionary = DialogueManager.get_current_extra_data()
	# TOP LEFT MESSAGE
	if extra_data.has("Top Left Message"):
		var front_layer : CanvasLayer = get_parent().owner.get_node("FrontLayer")
		var new_text : String = extra_data["Top Left Message"]
		front_layer.get_node("TopLeftBanner/Label").text = new_text
		front_layer.get_node("FrontAnimPlayer").play("top_left_anim")

	# PAUSE TIMER
	# TODO: Maybe don't have it pause when skipping
	if extra_data.has("Pause"):
		var pause_timer_time : String = extra_data["Pause"]
		pause_timer = float(pause_timer_time)

	if append_next_block:
		append_current_block = true
		append_next_block = false

	# COMMANDS
	for command in DialogueManager.get_commands():
		handle_command(command)

	# Force auto command if text is blank and not branching
	if DialogueManager.get_current_text() == "" and !DialogueManager.is_branching:
		auto_go_to_next = true

	# Finally do dialogue box stuff
	if append_current_block:
		$DialogueRichTextLabel.bbcode_text += get_current_bbcode_text()
		append_current_block = false
	else:
		visible_characters = 0
		$DialogueRichTextLabel.visible_characters = visible_characters
		$DialogueRichTextLabel.bbcode_text = get_current_bbcode_text()

	$CharacterLabel.text = DialogueManager.current_character

	if $CharacterLabel.text in ["-", "."]:
		$CharacterLabel.text = ""

func handle_command(command : String):
	command = command.strip_edges()
	# Get parameters
	var regex : RegEx = RegEx.new()
	var regex_str : String = "\\((.*)\\)"
	regex.compile(regex_str)
	var regex_match : RegExMatch = regex.search(command)
	var parameters : Array = []
	if regex_match != null:
		parameters = regex_match.strings[1].split(",")
		for param in parameters:
			param = param.strip_edges()
	# Remove parameters from command
	command = command.split("(")[0]
	command = handle_aliases(command)
	# Finally match the command
	match command:
		"hide": # hide(time, pause = true)
			visible = false
			if len(parameters) != 0:
				var hide_time: float = float(parameters[0])
				if len(parameters) < 2: # If no second parameter, make it true.
					parameters.append("true")
				# Pause if not specified and a longer pause timer is not already active
				if pause_timer < hide_time + 0.5 and parameters[1] == "true":
					pause_timer = hide_time + 0.5
				# Make visible after period of time
				yield(get_tree().create_timer(hide_time), "timeout")
				visible = true
		"show":
			visible = true
		"auto":
			auto_go_to_next = true
		"append":
			append_current_block = true
		"auto_append_next": # Same as having this block have auto and the next having append. A very useful shortcut.
			auto_go_to_next = true
			append_next_block = true
		"set": # Used to set custom variables. TODO: Allow these to be used
			DialogueManager.variable_dict[parameters[0]] = parameters[1]
		"increment":
			if len(parameters) < 2:
				parameters.append("1")
			var initial : String = DialogueManager.variable_dict[parameters[0]]
			var output : float = float(initial) + float(parameters[1])
			DialogueManager.variable_dict[parameters[0]] = output
		"decrement":
			if len(parameters) < 2:
				parameters.append("1")
			var initial : String = DialogueManager.variable_dict[parameters[0]]
			var output : float = float(initial) - float(parameters[1])
			DialogueManager.variable_dict[parameters[0]] = output
		"show_succulr":
			if !owner.get_node("FrontLayer/Succulr").visible:
				is_externally_paused = true
				#DialogueManager.is_branching = true
				owner.get_node("FrontLayer/Succulr").visible = true
				owner.get_node("FrontLayer/Succulr").initialise()
		"go_to_succulr_branch":
			var chosen_character_branch : String = "NOONE"
			if DialogueManager.has_var("succulr"):
				chosen_character_branch = DialogueManager.get_var("succulr").to_upper()
			DialogueManager.go_to_dialogue(chosen_character_branch)
		"fade_out_music":
			if len(parameters) == 0:
				MusicManager.fade_out()
			else:
				MusicManager.fade_out(float(parameters[0]))
		"fade_in_music":
			if len(parameters) == 0:
				MusicManager.fade_in()
			else:
				MusicManager.fade_in(float(parameters[0]))
		"play_sound":
			SoundManager.play_sound(parameters[0])
		"play_music":
			MusicManager.play_song(parameters[0])
		"set_music_behaviour":
			var behaviour : int = MusicManager.SHUFFLE
			if parameters.empty():
				parameters.append("shuffle")
			match parameters[0].to_lower():
				"shuffle":
					behaviour = MusicManager.SHUFFLE
				"once":
					behaviour = MusicManager.ONCE
				"repeat":
					behaviour = MusicManager.REPEAT
				
			MusicManager.set_behaviour(behaviour)
		"go_to_scene":
			var scene_str : String = parameters[0]
			SceneSwapper.swap_scene("res://scn/" + scene_str + ".tscn")
			DialogueManager.go_to_dialogue("START")
			visible = false
		_:
			push_warning("Unimplemented command: " + command)

func handle_aliases(command : String):
	match command:
		"play_song":
			return "play_music"
		"fade_music_in":
			return "fade_in_music"
		"fade_music_out":
			return "fade_out_music"
		"add":
			return "increment"
		"subtract":
			return "decrement"
	
	return command

func get_current_bbcode_text() -> String:
	return DialogueManager.get_current_text()

func is_blocked() -> bool: # Returns whether or not input is blocked by something else
	if (!OS.is_debug_build() || block_log_on_debug) && log_control.visible:
		return true
	return config_control.visible || !visible 