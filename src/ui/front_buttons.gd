extends Control
var front_buttons_arr : Array = []
# Called when the node enters the scene tree for the first time.
func _ready():
	front_buttons_arr = get_children()
	for element in front_buttons_arr:
		if !(element is Button):
			continue
		var btn : Button = element
		btn.connect("toggled", self, "on_button_toggled", [btn])
	clear_all_buttons()

func _input(event : InputEvent):
	var current_focus : Control = get_focus_owner()
	# Return if something important is in focus key being pressed is esc
	if current_focus is TextEdit or current_focus is LineEdit:
		if event is InputEventKey and current_focus.visible:
			var key_event : InputEventKey = event
			if !(key_event.scancode in [KEY_ESCAPE]):
				return
				
	if event.is_action_pressed("toggle_log"):
		var log_control : Control = owner.get_node("MenuLayer/Log")
		on_button_toggled(!log_control.visible, $LogButton) 
		$LogButton.pressed = log_control.visible
		
	if event.is_action_pressed("toggle_config"):
		var cfg_control : Control = owner.get_node("MenuLayer/Config")
		on_button_toggled(!cfg_control.visible, $ConfigButton)
		$ConfigButton.pressed = cfg_control.visible

func on_button_toggled(button_pressed, button):
	# Clear all buttons
	clear_all_buttons([button])
	# Finally set the property of this button 
	do_action_on_button(button_pressed, button)
	
	# Play sound
	owner.get_node("FrontUI/Choices/Sounds/hover").play()

func clear_all_buttons(exceptions : Array = []):
	for btn in front_buttons_arr:
		var previous_visibility : bool = btn.visible
		if btn in exceptions:
			btn.visible = false
			btn.visible = previous_visibility
			continue
		
		do_action_on_button(false, btn)
		btn.pressed = false
		
		btn.visible = false
		btn.visible = previous_visibility

func do_action_on_button(value, button, force_flip := false):
	var item = get_item_from_button(button)
	if force_flip:
		item.visible = !item.visible
	item.visible = value
	
func get_item_from_button(button):
	var menu_layer : Node = owner.get_node("MenuLayer")
	match button.name:
		"LogButton":
			return menu_layer.get_node("Log")
		"ConfigButton":
			return menu_layer.get_node("Config")
	